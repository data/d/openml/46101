# OpenML dataset: FitBit_Sleep

https://www.openml.org/d/46101

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Description:
The dataset named "minuteSleep_merged.csv" captures detailed sleep tracking information, segmented into one-minute intervals, for a group of users. It consists of several key attributes that enable a comprehensive analysis of sleep patterns over time.

Attribute Description:
- Id: A unique numeric identifier for each user in the dataset. Example values include 8378563200 and 4319703577, indicating distinct participants.
- date: The specific date and time when the sleep data was recorded, formatted as MM/DD/YYYY HH:MM:SS AM/PM. Sample entries, such as '4/5/2016 3:56:30 AM', represent when a user was detected as sleeping.
- value: A binary indicator showing whether the user was asleep (1) or awake (0) during the given minute. Most entries are '1', denoting a minute spent asleep.
- logId: A unique identifier for each sleep log entry. This number is critical for distinguishing between different sleep sessions for the same user. Examples include 11304567341 and 11327738294.

Use Case:
This dataset is invaluable for research in several areas, including sleep studies, healthcare analytics, and wearable technology performance. Analysts can use it to identify sleep patterns, assess sleep quality, and correlate sleep habits with potential health outcomes. Moreover, it's useful for developers testing algorithms on wearable devices that track sleep. By analyzing temporal sleep data, insights into sleep consistency, duration, and disturbances can be gleaned, offering a quantitative basis for improving sleep health recommendations.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46101) of an [OpenML dataset](https://www.openml.org/d/46101). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46101/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46101/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46101/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

